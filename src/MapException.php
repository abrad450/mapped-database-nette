<?php

namespace AbraD450\MappedDatabase;

/**
 * Map Exception
 */
class MapException extends \Exception
{
    const MAPPING_NOT_FOUND = 1;

    const ENTITY_CLASS_NOT_FOUND = 2;
}
