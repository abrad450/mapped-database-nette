<?php

namespace AbraD450\MappedDatabase;

use Nette\Neon\Neon;
use Nette\Utils\FileSystem;

/**
 * Config Helpers
 */
class ConfigHelpers
{
    public static function unifyConnection(object $config)
    {
        if(isset($config->entities->output) && isset($config->entities->config)) {
            // always array of entitySets
            $config->entities = [$config->entities];
        }
        foreach($config->entities as $index => $entitySet) {
            // read configuration file(s)
            if(is_string($entitySet->config)) {
                $config->entities[$index]->config = Neon::decode(FileSystem::read($entitySet->config));
            }
            elseif(is_array($entitySet->config) && !empty($entitySet->config) && is_string($entitySet->config[0])) {
                $configs = [];
                foreach($entitySet->config as $configFile) {
                    if(is_file($configFile)) {
                        $configs += Neon::decode(FileSystem::read($configFile));
                    }
                }
                $config->entities[$index]->config = $configs;
            }
        }
        return $config;
    }
}
