<?php

namespace AbraD450\MappedDatabase;

use Nette\Utils\Strings;

use Nette\Caching\Storage;
use Nette\Caching\Cache;

/**
 * Entity Map
 */
final class EntityMap implements \ArrayAccess
{
    /**
     * Hash map table => entity
     */
    private ?array $map = [];

    /**
     * Schema name
     */
    private ?string $schema;
    
    private Cache $cache;
            
    /**
     * Constructor
     * 
     * @param Storage $storage Cache Storage
     * @param string $connectionName
     * @param array $config Configuration array
     * @param string $schema Schema name
     */
    public function __construct(Storage $storage, string $connectionName, object $config, string $schema = NULL)
    {
        $this->cache = new Cache($storage, str_replace('\\', '.', get_called_class()));
        $this->loadMap($connectionName, $config);
        $this->schema = $schema;
    }
    
    public function loadMap(string $connectionName, object $config): void
    {
        if(\Tracy\Debugger::$productionMode) {
            $this->map = $this->cache->load($connectionName);
            if(isset($this->map)) {
                return;
            }
        }
        
        $cfg = ConfigHelpers::unifyConnection($config);
        $this->map = [];
        foreach($cfg->entities as $entitySet) {
            foreach($entitySet->config as $tableName => $tableConfig) {
                $this->map[$tableName] = $entitySet->namespace.'\\'.$tableConfig['class'];
            }
        }

        $this->cache->save($connectionName, $this->map);
    }
    
    /**
     * Append next map on top of existing
     * 
     * @param array $map
     */
    public function append(array $map)
    {
        foreach($map as $table => $entity) {
            $this->map[$table] = $entity;
        }
    }

    public function offsetExists(mixed $offset): bool
    {
        $rOffset = $this->removeSchema($offset);
        return isset($this->map[$rOffset]);
    }

    public function offsetGet(mixed $offset): mixed
    {
        $rOffset = $this->removeSchema($offset);
        if(!$this->offsetExists($rOffset)) {
            throw new InvalidArgumentException("Mapping for table '{$rOffset}' not found!");
        }
        return $this->map[$rOffset];
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        $rOffset = $this->removeSchema($offset);
        $this->map[$rOffset] = $value;
    }

    public function offsetUnset(mixed $offset): void
    {
        $rOffset = $this->removeSchema($offset);
        unset($this->map[$rOffset]);
    }

    
    
    /**
     * Removes schema (if given) from $offset
     * 
     * @param string $offset
     * @return string
     */
    protected function removeSchema(string $offset): string
    {
        if($this->schema && Strings::startsWith(Strings::lower($offset), Strings::lower($this->schema).'.')) {
            return Strings::substring($offset, Strings::length($this->schema) + 1);
        }
        return $offset;
    }
    
}
