<?php

namespace AbraD450\MappedDatabase\Entity;

/**
 * Entity Callback Iterator
 */
class EntityCallbackIterator implements \Iterator
{
    /**
     * @var \Iterator
     */
    protected $iterator;
    
    /**
     * @var callable
     */
    protected $callback;

    /**
     * Constructor
     * 
     * @param \Iterator $iterator
     * @param callable $callback
     */
    public function __construct(\Iterator $iterator, callable $callback)
    {
        $this->iterator = $iterator;
        $this->callback = $callback;
    }

    public function current()
    {
        $val = $this->iterator->current();
        $call = $this->callback;
        return $call($val, $this->key(), $this);
    }

    public function key()
    {
        return $this->iterator->key();
    }

    public function next(): void
    {
        $this->iterator->next();
    }

    public function rewind(): void
    {
        $this->iterator->rewind();
    }

    public function valid(): bool
    {
        return $this->iterator->valid();
    }

}
