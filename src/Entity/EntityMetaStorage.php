<?php

namespace AbraD450\MappedDatabase\Entity;

use Nette;
use Nette\Caching\Storage;
use Nette\Caching\Cache;

/**
 * Entity Metadata Storage
 */
class EntityMetaStorage
{
    
    /**
     * Cache storage
     */
    private static Storage $storage;
    
    /**
     * Cache
     */
    private static Cache $cache;
    
    /**
     * Config
     */
    private static \stdClass $config;
    
    /**
     * Was storage initialized ?
     */
    private static bool $initialized = FALSE;
    
    /**
     * Initialize storage (called from DI, when DB Connection is created)
     * 
     * @param type $config
     * @param Storage $storage
     */
    public static function initialize($config, Storage $storage)
    {
        if(!self::$initialized) {
            self::$storage = $storage;
            self::$cache = new Cache($storage, str_replace('\\', '.', get_called_class()));
            self::$config = $config;
            self::$initialized = TRUE;
        }
    }
    
    /**
     * Describe entity
     * 
     * @param string $class
     * @return EntityMeta
     */
    public static function describe($class)
    {
        if(empty(self::$cache)) {
            throw new Nette\InvalidStateException('Cache for entity metadata is not available!');
        }
        
        // Read cache
        $cached = self::$cache->load($class);
        if($cached !== NULL) {
            if(self::$config->cacheMeta) {
                return $cached;
            }
            // Compare last modified timestamp
            $rc = new \ReflectionClass($class);
            if($cached->lastModified == filemtime($rc->getFileName())) {
                return $cached;
            }
        }        
        
        // Cache it
        $entityMeta = self::describeEntity($class);
        self::$cache->save($class, $entityMeta);
        
        return $entityMeta;
    }
    
    
    
    protected static function describeEntity($class)
    {
        $refClass = new \ReflectionClass($class);
        
        $entity = new \stdClass();
        $entity->name = $class;
        $entity->file = $refClass->getFileName();
        $entity->lastModified = filemtime($refClass->getFileName());
        $entity->properties = [];
        $entity->searchables = [];
                
        // Table
        $tableAttrs = $refClass->getAttributes(Attributes\Table::class);
        foreach($tableAttrs as $tableAttr) {
            $entity->table = $tableAttr->newInstance()->getName();
        }
        
        // Property
        $propAttrs = $refClass->getAttributes(Attributes\Property::class);
        foreach($propAttrs as $propAttr) {
            self::setPropertyAttribute($entity, $propAttr->newInstance());
        }

        return $entity;
    }
    
    
    protected static function setPropertyAttribute(\stdClass $entity, Attributes\Property $property)
    {
        $pName = $property->getName();
        $pCol = $property->getCol();

        $attrs = $property->getProps();
        $entity->properties[$pName] = $attrs;
        
        if(empty($property->ref) && empty($property->related) && empty($property->virtual)) {
            $entity->columns[$pCol] = $attrs;
        }
        
        if($property->isSearchable()) {
            $entity->searchables[$pName] = $pCol;
        }
    }

    
}
