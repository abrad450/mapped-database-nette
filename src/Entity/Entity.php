<?php

namespace AbraD450\MappedDatabase\Entity;

use Nette;
use Nette\InvalidArgumentException;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\GroupedSelection;
use Nette\Database\Table\Selection;

/**
 * Entity
 */
abstract class Entity extends ActiveRow implements \JsonSerializable
{
    /** Manually construct entity using property names only */
    const ENTITY_CONSTRUCT_PROPERTY = 'property';
    
    /** Manually construct entity using DB columns only */
    const ENTITY_CONSTRUCT_COLUMN = 'column';
    
    /** Manually construct entity using both property names and DB columns */
    const ENTITY_CONSTRUCT_AUTO = 'auto';
    
    
    use Nette\SmartObject { 
        __get as smartObject__get;
        __set as smartObject__set;
        __isset as smartObject__isset;
        __unset as smartObject__unset;
    }
       
    /**
     * DateTime, Time, Date formats
     * @var string[]
     */
    protected static $dateTimeFormats = [
        // DATETIME
        'Y-m-d H:i:s',
        'Y-m-d\TH:i:s.u\Z',
        DATE_ATOM,
        DATE_COOKIE,
        DATE_ISO8601,
        DATE_RFC1036,
        DATE_RFC1123,
        DATE_RFC2822,
        DATE_RFC3339,
        DATE_RFC822,
        DATE_RFC850,
        DATE_RSS,
        DATE_W3C,
        'Y-m-d H:i',
        // TIME
        'H:i:s',
        'H:i',
        'G:i:s',
        'G:i',
        'H:i:s.u',
        'H:i.u',
        'G:i:s.u',
        'G:i.u',
        // DATE
        'Y-m-d',
        'Y-n-j',
        'Y-m-dP'
    ];
    
    
    /**
     * Entity metadata
     * 
     * @var \stdClass
     */
    protected $meta;
    
    /**
     * Modified data
     * 
     * @var array
     */
    protected $modified;
    
    /**
     * Extra DB Fields from Selection
     * 
     * @var array
     */
    protected $extraFields;
    
    /**
     * Is this object bound to selection
     * 
     * @var bool
     */
    protected $isBoundToSelection;
    
    /**
     *
     * @var Selection
     */
    protected $boundSelection;
    
    /**
     * Keys which were given to the constructor's parameter $data
     * 
     * @var array Hashmap
     */
    protected $constructKeys;
    
    
    /**
     * Returns entity metadata
     * 
     * @return \stdClass
     */
    public function getEntityMetadata(): \stdClass
    {
        return $this->meta;
    }
        

    /**
     * Constructor
     * 
     * @param array|null $data Data (either from DB or manually entered)
     * @param Selection|null $selection Selection (when constructed from DB)
     * @param bool $keys If constructing manually: 'property' = find $data keys in properties only, 'column' = find $data keys in columns only, 'auto' = try both properties and columns)
     * @return type
     */
    public function __construct(?array $data = NULL, ?Selection $selection = NULL, string $keys = self::ENTITY_CONSTRUCT_PROPERTY)
    {
        $this->meta = EntityMetaStorage::describe(get_called_class());
        $this->modified = [];
        $this->extraFields = [];

        // Selection given
        if(is_array($data) && $selection instanceof Selection) {
            $this->isBoundToSelection = TRUE;
            $this->boundSelection = $selection;
            $constructFields = $this->colToFieldArray(array_keys($data));
            $this->constructKeys = array_fill_keys($constructFields, TRUE);
            
            // decodeNativeTypes from DB
            foreach($data as $dbField => $dbValue) {
                $fieldMeta = $this->meta->columns[$dbField] ?? NULL;
                if(!empty($fieldMeta)) {
                    $data[$dbField] = $this->decodeNativeTypes($fieldMeta->name, $dbValue, $fieldMeta);
                }
                else {
                    $this->extraFields[$dbField] = true;
                }
            }
            
            // ActiveRow constructor and end
            parent::__construct($data, $selection);
            return;
        }

        // no selection given
        $this->isBoundToSelection = FALSE;
        if(!is_array($data)) {
            return;
        }
        
        $canProp = $keys === self::ENTITY_CONSTRUCT_PROPERTY || $keys === self::ENTITY_CONSTRUCT_AUTO;
        $canCol = $keys === self::ENTITY_CONSTRUCT_COLUMN || $keys === self::ENTITY_CONSTRUCT_AUTO;

        foreach($data as $prop => $val) {
            
            if($canProp && isset($this->meta->properties[$prop])) {
                // may be property 
                $propName = $this->meta->properties[$prop]->name;
            }
            elseif($canCol && isset($this->meta->columns[$prop])) {
                // or may be column 
                $propName = $this->meta->columns[$prop]->name;
            }
            else {
                // neither property nor column? skip field
                continue;
            }
            
            // property is read-only
            if($this->meta->properties[$propName]->access === 'r') { 
                continue;
            }
            
            $this->__set($propName, $val);
        }
    }
    
    
    /**
     * Parses date/datetime value from string
     * 
     * @param string $value
     * @param string|null $tryFormat Try this format first
     * @return \Nette\DateTime
     */
    protected function parseDateTime(string $value, ?string $tryFormat = NULL): \Nette\Utils\DateTime
    {
        if($tryFormat !== NULL) {
            $dt = \Nette\Utils\DateTime::createFromFormat($tryFormat, $value);
            if($dt) {
                return $dt;
            }
        }
        foreach(self::$dateTimeFormats as $format) {
            $dt = \Nette\Utils\DateTime::createFromFormat($format, $value);
            if($dt) {
                return $dt;
            }
        }
        throw new EntityException('Unable to parse DateTime string "'.$value.'"', EntityException::INVALID_FORMAT);
    }    
    
    /**
     * Convert property to correct datatype defined on entity
     * 
     * @param string $property Property name
     * @param mixed $value Property value
     * @return mixed
     */
    protected function convertPropertyDataType(string $property, $value)
    {
        if($value === NULL) {
            return $value;
        }
        $someType = $this->meta->properties[$property]->nativeType ?? $this->meta->properties[$property]->type;
        switch(strtolower($someType)) {
            case 'real':
            case 'float':
            case 'double':
            case 'decimal':
                if(!is_scalar($value)) {
                    throw new EntityException("Unable to set '".gettype($value)."' for the property '{$property}'. Value should be of type 'float'", EntityException::INVALID_FORMAT);
                }
                return floatval($value);
            case 'uint':
            case 'int':
            case 'int unsigned':
            case 'tinyint':
            case 'tinyint unsigned':
            case 'smallint':
            case 'smallint unsigned':
            case 'mediumint':
            case 'mediumint unsigned':
            case 'bigint':
            case 'bigint unsigned':
            case 'integer':
            case 'integer unsigned':
                if(!is_scalar($value)) {
                    throw new EntityException("Unable to set '".gettype($value)."' for the property '{$property}'. Value should be of type 'integer'", EntityException::INVALID_FORMAT);
                }
                return intval($value);
            case 'string':
            case 'text':
                if(!is_scalar($value)) {
                    throw new EntityException("Unable to set '".gettype($value)."' for the property '{$property}'. Value should be of type 'string'", EntityException::INVALID_FORMAT);
                }
                return strval($value);
            case 'bit':
            case 'bool':
            case 'boolean':
                if(!is_scalar($value)) {
                    throw new EntityException("Unable to set '".gettype($value)."' for the property '{$property}'. Value should be of type 'boolean'", EntityException::INVALID_FORMAT);
                }
                return boolval($value);
            case 'date':
            case 'datetime':
            case '\\datetime':
            case 'timestamp':
                if($value instanceof \DateTimeInterface) {
                    if($value instanceof Nette\Utils\DateTime) {
                        return $value;
                    }
                    return Nette\Utils\DateTime::from($value);
                }
                else {
                    $format = $this->meta->properties[$property]->format;
                    return $this->parseDateTime($value, $format);
                }
            case 'time':
                if($value instanceof \DateInterval) {
                    return Nette\Utils\DateTime::createFromFormat('H:i:s', $value->format('%H:%I:%S'));
                }
                else {
                    $format = $this->meta->properties[$property]->format;
                    return $this->parseDateTime($value, $format);
                }
            case 'object':
                $type = gettype($value);
                
                if($type === 'resource') {
                    return stream_get_contents($value);
                }
                
                return $value;
            default:
                return $value;
        }
    }
    
    /**
     * Property getter
     * 
     * @param string $key Property name
     * @return mixed
     * @throws Nette\MemberAccessException
     * @throws EntityException
     */
    public function &__get(string $key): mixed
    {
        // If requested $key is not property but column (because it might be called from Nette's ActivityRow / GroupedSelection ...)
        // ... then convert column to field (I know, it might result in name clash, but I don't know if it can be done better)
        if(!isset($this->meta->properties[$key]) && isset($this->meta->columns[$key])) {
            $key = $this->meta->columns[$key]->name;
        }
        
        if(!isset($this->meta->properties[$key])) {
            // Might be selected as extra field
            try {
                return parent::__get($key);
            }
            catch(Nette\MemberAccessException $mae) {
                throw new Nette\MemberAccessException('Attempt to read non-existent property "'.$key.'"', previous: $mae);
            }
        }
        $prop = $this->meta->properties[$key];
        if($prop->access === 'w') {
            throw new Nette\MemberAccessException('Attempt to read write-only property "'.$key.'"');
        }
        
        // Virtual
        if($prop->virtual) {
            return $this->smartObject__get($key);
        }
        
        if(array_key_exists($key, $this->modified)) {
            return $this->modified[$key];
        }        
        
        // value is returned be reference
        $value = NULL;
        
        // if we have no selection, we end here
        if(!$this->isBoundToSelection) {
            return $value;
        }
        // If "ref" column was requested
        if(!empty($prop->ref)) {
            // test if "through" column has a value (is not null)...
            $throughValue = $this->__get($this->colToField($prop->through));
            if($throughValue === NULL) {
                return $value;  // return NULL
            }
            // it has value, go through the reference
            $value = $this->ref($prop->ref, $prop->through);
        }
        elseif(!empty($prop->related)) {
            $value = $this->related($prop->related, $prop->through);
        }
        elseif(isset($this->constructKeys[$key])) {
            // only if it exists in constructKeys (it was in data (might not be SELECTed) on object construction)
            $value = $this->convertPropertyDataType($key, parent::__get($this->fieldToCol($key)));
        }
        elseif(count($this->boundSelection->getSqlBuilder()->getSelect()) <= 0) {
            // might be cached old SELECT, maybe needs to recache (only if nothing was selected)
//            \Tracy\Debugger::barDump($key, 'xKey');
//            \Tracy\Debugger::barDump($this->fieldToCol($key), 'xCol');
            $value = $this->convertPropertyDataType($key, parent::__get($this->fieldToCol($key)));
        }
        
        return $value;
    }
    
    /**
     * Property setter
     * 
     * @param string  $key Property name
     * @param mixed $value
     * @throws Nette\MemberAccessException
     * @throws EntityException
     */
    public function __set($key, $value): void
    {
        if(!isset($this->meta->properties[$key])) {
            throw new Nette\MemberAccessException('Attempt to write to non-existent property "'.$key.'"');
        }
        
        $prop = $this->meta->properties[$key];
        
        if($prop->access === 'r') {
            throw new Nette\MemberAccessException('Attempt to write to read-only property "'.$key.'"');
        }
        
        // Virtual
        if($prop->virtual) {
            $this->smartObject__set($key);
        }
        else {
            $this->modified[$key] = $this->convertPropertyDataType($key, $value);
        }
    }
    
    /**
     * Is property set ?
     * 
     * @param string $key
     * @return bool
     */
    public function __isset($key): bool
    {
        if(!isset($this->meta->properties[$key])) {
            return FALSE;
        }
        
        if($this->meta->properties[$key]->virtual) {
            return $this->smartObject__isset($key);
        }
        
        if(isset($this->modified[$key])) {
            return TRUE;
        }
        
        if(!$this->isBoundToSelection) {
            return FALSE;
        }
        
        $meta = $this->meta->properties[$key];
        // if this is NEITHER ref NOR related property - convert to field to col
        if(!isset($meta->ref) && !isset($meta->related) && parent::__isset($this->fieldToCol($key))) {
            return TRUE;
        }
        
        // if this is REF property - use through
        if(isset($meta->ref) && parent::__isset($meta->through)) { // parent::__isset($key)
            return TRUE;
        }
        
        // if this is RELATED property - Selection => always true
        if(isset($meta->related)) { // parent::__isset($key)
            return TRUE;
        }
        
        return FALSE;
    }
    

    /**
     * Unset property (only modified)
     * 
     * @param string $key
     */
    public function __unset($key): never
    {
        if(!isset($this->meta->properties[$key])) {
            //return;
        }
        elseif($this->meta->properties[$key]->virtual) {
            $this->smartObject__unset($key);
            //return;
        }
        else {
            unset($this->modified[$key]);
        }
        // This is never called on parent => throws an exception
    }
    
    
    /**
     * Get modified properties array
     * 
     * @return array
     */
    public function getModifiedArray(): array
    {
        return $this->modified;
    }
    
    /**
     * Get modified columns array
     * 
     * @return array
     */
    public function getModifiedArrayDb(): array
    {
        return $this->fieldToColAssoc($this->modified, [$this, 'encodeNativeTypes']);
    }
    
    /**
     * Returns all direct properties as array
     * 
     * @param callable|null $callback ($value, $prop, $propMeta)
     * @param bool $constructKeysOnly Return array with construct keys only (useful if SELECT col,col2,col3 is present)
     * @param bool $appendExtraFields
     * @param bool $appendVirtualFields
     * @return array
     */
    public function toArray(?callable $callback = NULL, $constructKeysOnly = TRUE, $appendExtraFields = FALSE, $appendVirtualFields = FALSE): array
    {
        $array = [];
        foreach($this->meta->properties as $prop => $propMeta) {
            $isDirect = empty($propMeta->ref) && empty($propMeta->related);
            if($propMeta->access !== 'w' && $isDirect && (!$constructKeysOnly || isset($this->constructKeys[$prop]) || ($propMeta->virtual && $appendVirtualFields))) {
                $value = $this->__get($prop);
                $array[$prop] = $callback ? $callback($value, $prop, $propMeta) : $value;
            }
        }
        if($appendExtraFields) {
            foreach($this->extraFields as $field => $true) {
                $value = $this->__get($field);
                $array[$field] = $callback ? $callback($value, $field, null) : $value;
            }
        }
        return $array;
    }
    
    /**
     * Returns all direct columns as array
     * 
     * @param callable|null $callback
     * @param bool $constructKeysOnly
     * @param bool $appendExtraFields
     * @return array
     */
    public function toArrayDb(?callable $callback = NULL, $constructKeysOnly = TRUE, $appendExtraFields = FALSE): array
    {
        return $this->fieldToColAssoc($this->toArray($callback, $constructKeysOnly, $appendExtraFields), [$this, 'encodeNativeTypes']);
    }
        
    /**
     * Returns primary key values
     * 
     * @param bool $throw
     * @return mixed (might be single value or for complex primary keys array)
     */
    public function getPrimary(bool $throw = true): mixed
    {
        if(!$this->isBoundToSelection) {
            $array = [];
            foreach($this->meta->properties as $prop => $propMeta) {
                if($propMeta->key) {
                    $value = $this->__get($prop);
                    if($value === NULL && $throw) { 
                        throw new Nette\InvalidStateException(
                                "Entity does not contain value for primary key property \"{$prop}\""
                            );
                    }
                    $array[$prop] = $value;
                }
            }
            if(count($array) > 1) {
                return $array;
            }
            return reset($array);
        }
        $dbPrimary = parent::getPrimary($throw);
        if(is_array($dbPrimary)) {
            // 2023-01-26 - Do not convert to fields, it is used by update() !
            return $dbPrimary;
            // return $this->colToFieldAssoc($dbPrimary);
        }
        return $dbPrimary;
    }
    
    
	/**
	 * Returns referenced row.
	 * @return self|null if the row does not exist
	 */
	public function ref(string $key, ?string $throughColumn = null): ?ActiveRow
	{
        if(!$this->isBoundToSelection) {
            throw new EntityException('This method can only be called when Entity is created from Selection', EntityException::INVALID_CALL);
        }
        return parent::ref($key, $throughColumn);
	}


	/**
	 * Returns referencing rows.
	 */
	public function related(string $key, ?string $throughColumn = null): GroupedSelection
	{
        if(!$this->isBoundToSelection) {
            throw new EntityException('This method can only be called when Entity is created from Selection', EntityException::INVALID_CALL);
        }
		return parent::related($key, $throughColumn);
	}    
    
    /**
     * Save modified data to database
     * Must be connected to Selection (e.g. created from database)
     * 
     * @throws EntityException
     */
    public function saveModified()
    {
        if(!$this->isBoundToSelection) {
            throw new EntityException('This method can only be called when Entity is created from Selection', EntityException::INVALID_CALL);
        }
        $updated = parent::update($this->fieldToColAssoc($this->modified, [$this, 'encodeNativeTypes']));
        $this->modified = [];
        return $updated;
    }
    
    /**
     * Updates an entity with iterable data (does not use the 'modified' data)
     * If no data was updated, returns false.
     * Invalid keys (properties) will be ignored
     * 
     * @param iterable $data
     * @return bool
     */
    public function update(iterable $data): bool
    {
        if($this->isBoundToSelection) {
            return parent::update($this->fieldToColAssoc($data, [$this, 'encodeNativeTypes']));
        }
        return false;
    }
    
    
    public function delete(): int
    {
        if(!$this->isBoundToSelection) {
            throw new EntityException('This method can only be called when Entity is created from Selection', EntityException::INVALID_CALL);
        }
        return parent::delete();
    }
    
    
    public function getIterator(): \Iterator
    {
        if(!$this->isBoundToSelection) {
            return new \ArrayIterator($this->modified);
        }
        // $iterator = parent::getIterator();
        $array = [];
        foreach($this->meta->properties as $key => $prop) {
            if($prop->access !== 'w') {
                $array[$key] = $prop;
            }
        }
        $iterator = new \ArrayIterator($array);
        $entityIterator = new EntityCallbackIterator($iterator, function($value, $key) {
            return $this->__get($key);
        });
        return $entityIterator;
    }
    
    
    public function accessColumn($key, bool $selectColumn = true): bool
    {
        if(!$this->isBoundToSelection) {
            throw new EntityException('This method can only be called when Entity is created from Selection', EntityException::INVALID_CALL);
        }
        return parent::accessColumn($key, $selectColumn);
    }
    
    public function getTable(): Selection
    {
        if(!$this->isBoundToSelection) {
            throw new EntityException('This method can only be called when Entity is created from Selection', EntityException::INVALID_CALL);
        }
        return parent::getTable();
    }
    
    public function setTable(Selection $table): void
    {
        if(!$this->isBoundToSelection) {
            throw new EntityException('This method can only be called when Entity is created from Selection', EntityException::INVALID_CALL);
        }
        parent::setTable($table);
    }

    public function jsonSerialize(): mixed
    {
        return $this->toArray();
    }
    
    public function getConstructKeys($asKeys = FALSE)
    {
        return $asKeys ? $this->constructKeys : array_keys($this->constructKeys);
    }
    
    
    
    // --------------------------------------------------------------------------------
    // Fields to Cols
    // --------------------------------------------------------------------------------
    
    protected function fieldToCol(string $field)
    {
        if(!isset($this->meta->properties[$field])) {
            throw new InvalidArgumentException("Field '{$field}' was not found in metadata");
        }
        return $this->meta->properties[$field]->col;
    }
    
    protected function colToField(string $col)
    {
        if(!isset($this->meta->columns[$col])) {
            throw new InvalidArgumentException("Column '{$col}' was not found in metadata");
        }
        return $this->meta->columns[$col]->name;
    }    
    
    protected function fieldToColAssoc(array $fields, ?callable $callback = NULL)
    {
        $cols = [];
        foreach($fields as $field => $val) {
            if(!isset($this->meta->properties[$field])) {
                continue;
            }
                
            if($this->meta->properties[$field]->related || $this->meta->properties[$field]->ref || $this->meta->properties[$field]->virtual) {
                continue;
            }
                
            if($callback) {
                $cols[$this->fieldToCol($field)] = $callback($field, $val, $this->meta->properties[$field]);
            }
            else {
                $cols[$this->fieldToCol($field)] = $val;
            }
        }
        return $cols;
    }
    
    protected function colToFieldAssoc(array $cols)
    {
        $fields = [];
        foreach($cols as $col => $val) {
            if(isset($this->meta->columns[$col])) {
                $fields[$this->colToField($col)] = $val;
            }
        }
        return $fields;
    }

    protected function colToFieldArray(array $cols)
    {
        $fields = [];
        foreach($cols as $col) {
            if(isset($this->meta->columns[$col])) {
                $fields[] = $this->colToField($col);
            }
        }
        return $fields;
    }

    
    /**
     * Convert native types (for example encodes JSON fields)
     * 
     * @param string $field
     * @param mixed $val
     * @param Attributes\Property $meta
     */
    protected function encodeNativeTypes(string $field, mixed $val, Attributes\Property|\stdClass $meta)
    {
        if($val === NULL) {
            return $val;
        }
        
        switch($meta->nativeType) {
            case 'json':
            case 'jsonb':
                return Nette\Utils\Json::encode($val);
        }
        return $val;
    }
    
    /**
     * Decode native types
     * 
     * @param string $field
     * @param mixed $val
     * @param Attributes\Property $meta
     * @return mixed
     */
    protected function decodeNativeTypes(string $field, mixed $val, Attributes\Property|\stdClass $meta)
    {
        if($val === NULL) {
            return $val;
        }
        
        switch($meta->nativeType) {
            case 'json':
            case 'jsonb':
                return Nette\Utils\Json::decode($val, ($meta->json ?? 'array') === 'array');
        }
        return $val;        
    }
    
}