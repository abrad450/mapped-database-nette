<?php
namespace AbraD450\MappedDatabase\Entity\Attributes;

use Attribute;

/**
 * Table attribute
 */
#[Attribute(Attribute::TARGET_CLASS)]
class Table
{
    private string $name;
    
    public function __construct(string $name)
    {
        $this->name = $name;
    }
    
    public function getName(): string
    {
        return $this->name;
    }
}
