<?php

namespace AbraD450\MappedDatabase\Entity;

/**
 * Entity Exception
 */
class EntityException extends \Exception
{
    const INVALID_FORMAT = 1;
    
    const INVALID_CALL = 2;
}
