<?php

namespace AbraD450\MappedDatabase;

use Nette\Database\Connection;
use Nette\Database\Structure;
use Nette\Database\Conventions;

use Nette\Caching\Storage;

/**
 * Mapped Explorer
 * 
 * @property-read EntityMap $entityMap
 */
class Explorer extends \Nette\Database\Explorer
{
    
    protected $config;
    
    protected $connectionName;
    
    /**
     *
     * @var EntityMap
     */
    protected $entityMap;
    
    
    protected $_conventions;
    
    protected $_cacheStorage;
    
    
    /**
     * Get entity map
     *
     * @return EntityMap
     */
    public function getEntityMap(): EntityMap
    {
        return $this->entityMap;
    }
    
    /**
     * Constructor
     * 
     * @param string $connectionName
     * @param array $config
     * @param Connection $connection
     * @param Structure $structure
     * @param Conventions $conventions
     * @param \Nette\Caching\IStorage $cacheStorage
     */
    public function __construct(
            $connectionName,
            $config, 
            Connection $connection, 
            Structure $structure, 
            Conventions $conventions = null, 
            Storage $cacheStorage = null)
    {
        parent::__construct($connection, $structure, $conventions, $cacheStorage);
        $this->config = $config;
        $this->connectionName = $connectionName;
        
        $this->entityMap = new EntityMap($cacheStorage, $connectionName, $config, $config->schema);
        
        $this->_conventions = $conventions;
        $this->_cacheStorage = $cacheStorage;
    }
    
	public function table(string $table): \Nette\Database\Table\Selection
	{
		return new Table\Selection($this->entityMap, $this, $this->_conventions, $table, $this->_cacheStorage);
	}
    
}
