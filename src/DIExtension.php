<?php

namespace AbraD450\MappedDatabase;

use Nette;
use Nette\Schema\Expect;

/**
 * DI Extension
 */
class DIExtension extends Nette\DI\CompilerExtension
{

	public function getConfigSchema(): Nette\Schema\Schema
	{
		return Expect::structure([
            'cacheMeta' => Expect::bool(true)->required(false),
            'connections' => Expect::arrayOf(
                Expect::structure([
                    'schema' => Expect::string()->required(false),
                    'entities' => Expect::anyOf(
                        Expect::arrayOf(
                            Expect::structure([
                                'namespace' => Expect::string()->required(true),
                                'output' => Expect::string()->required(true),
                                'config' => Expect::anyOf(
                                    Expect::string(),
                                    Expect::arrayOf(Expect::string())
                                )->required(true),
                                'cleanSkip' => Expect::arrayOf(Expect::string())->required(false),
                                'onClass' => Expect::array(),
                                'onDataType' => Expect::array()
                            ])
                        ),
                        Expect::structure([
                            'namespace' => Expect::string()->required(true),
                            'output' => Expect::string()->required(true),
                            'config' => Expect::anyOf(
                                Expect::string(),
                                Expect::arrayOf(Expect::string())
                            )->required(true),
                            'cleanSkip' => Expect::arrayOf(Expect::string())->required(false),
                            'onClass' => Expect::array(),
                            'onDataType' => Expect::array()
                        ])
                    )->required(true)
                    //'entityMap' => Expect::array()->required(true)
                ])
            )->required(true)
		]);
	}    
    
    
    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();

        foreach ($this->config->connections as $name => $config) {
            $this->setupDatabase($builder, $config, $name);
        }
        
        $builder->addDefinition($this->prefix("entitygenerator"))
            ->setFactory(Generator\EntityGenerator::class, [$this->config])
            ->setAutowired(TRUE);        
    }
    
    
    public function setupDatabase(\Nette\DI\ContainerBuilder $builder, $config, $name)
    {
        $explorer = $builder->getDefinition("database.{$name}.explorer");
        $isAutowired = $explorer->getAutowired();
        
        $args = $explorer->getFactory()->arguments;
        array_unshift($args, $name, $config);

        $builder->removeDefinition("database.{$name}.explorer");
        $builder->addDefinition("database.{$name}.explorer")
            ->setFactory(Explorer::class, $args)
            ->setAutowired($isAutowired);        
    }
    
    
	public function beforeCompile()
	{
		$builder = $this->getContainerBuilder();
        
		foreach ($this->config->connections as $name => $config) {
            $connection = $builder->getDefinition("database.{$name}.connection");
            $connection->addSetup([Entity\EntityMetaStorage::class, 'initialize'], [$this->config, '@cache.storage']);
		}                
	}

}
