<?php

namespace AbraD450\MappedDatabase\Generator;

use Latte\Sandbox\Nodes\PropertyFetchNode;
use Nette;
use Nette\Database\Explorer;
use Nette\Utils\Strings;

use AbraD450\MappedDatabase\Entity\Attributes;
use AbraD450\MappedDatabase\Generator\Adapters\GeneratorAdapter;

/**
 * Entity Generator
 */
class EntityGenerator
{
    use Nette\SmartObject;

    protected array $connections;
    
    protected Nette\DI\Container $container;
    
    /**
     * Hashmap of GeneratorAdapters [type => instance]
     */
    protected array $adapters;

    /**
     * Event fired when class is about to be generated
     * @var callable[]
     */
    public $onClass;

    /**
     * Event fired when PHP field type is about to be set based on DB type
     * @var callable[]
     */
    public $onDataType;
    
    
    public function __construct(\stdClass $config, Nette\DI\Container $container)
    {
        $this->connections = $config->connections;
        
        foreach($this->connections as $connName => $connConfig) {
            $this->connections[$connName] = \AbraD450\MappedDatabase\ConfigHelpers::unifyConnection($connConfig);
        }
        
        $this->container = $container;
    }
    
    
    /**
     * Generate entities
     * 
     * @param Explorer $db DB explorer
     * @param array $configuration All entities configuration
     * @param string $dir Output directory for entities
     * @param string $table If only entity for one table should be generated
     * @return array Array of generated files
     */
    public function generate(): array
    {
        $cleaned = [];
        
        $written = [];
        foreach($this->connections as $connName => $connConfig) {
            $db = $this->container->getService("database.{$connName}.explorer");
            foreach($connConfig->entities as $entitySet) {

                // onClass binding
                $this->onClass = [];
                if(isset($entitySet->onClass) && is_array($entitySet->onClass) && count($entitySet->onClass) == 2) {
                    $onClassService = $this->container->getByType($entitySet->onClass[0]);
                    $this->onClass[] = [$onClassService, $entitySet->onClass[1]];
                }

                // onDataType binding
                $this->onDataType = [];
                if(isset($entitySet->onDataType) && is_array($entitySet->onDataType) && count($entitySet->onDataType) == 2) {
                    $onDataTypeService = $this->container->getByType($entitySet->onDataType[0]);
                    $this->onDataType[] = [$onDataTypeService, $entitySet->onDataType[1]];
                }
                
                foreach($entitySet->config as $tName => $tConfig) {
                    
                    $outputDir = rtrim($entitySet->output, '/\\');
                    if(!isset($cleaned[$outputDir])) {
                        
                        $files = glob($outputDir.'/*.php');
                        foreach($files as $file) {

                            // Should this file be skipped?
                            $fileSlash = Strings::lower(str_replace('\\', '/', $file));
                            $skip = false;
                            if(isset($entitySet->cleanSkip) && is_array($entitySet->cleanSkip)) {
                                foreach($entitySet->cleanSkip as $skipName) {
                                    $skipLowerName = Strings::lower($skipName);
                                    if(str_ends_with($fileSlash, '/'.$skipLowerName)) {
                                        $skip = true;
                                        break;
                                    }
                                }
                            }
                            // Delete the old file (if it is not skipped)
                            if(!$skip) {
                                unlink($file);
                            }
                        }                        
                        
                        $cleaned[$outputDir] = true;
                    }
                    
                    // Generate content
                    $classContent = $this->generateClassContent($db, $entitySet->namespace, $connName, $tName, $tConfig);
                    
                    // Save it to file
                    $written[] = $this->writeClassFile($classContent, $outputDir);
                }
            }
        }
        return $written;
    }
    
    protected function getGeneratorAdapter(Explorer $db): GeneratorAdapter
    {
        $type = explode(':', $db->getConnection()->getDsn())[0];

        $adapterClass = __NAMESPACE__.'\\Adapters\\'.ucfirst($type).'GeneratorAdapter';
        if(!class_exists($adapterClass)) {
            $adapterClass =__NAMESPACE__.'\\Adapters\\GenericGeneratorAdapter';
        }

        if(!empty($this->adapters[$type])) {
            return $this->adapters[$type];
        }

        $this->adapters[$type] = new $adapterClass();
        return $this->adapters[$type];
    }
    
    protected function generateClassContent(Explorer $db, string $namespace, string $connectionName, string $table, array $tConfig): \stdClass
    {
        $adapter = $this->getGeneratorAdapter($db);

        if(!isset($tConfig['class'])) {
            throw new Nette\InvalidArgumentException("Missing key 'class' for table '{$table}' definition.");
        }
        
        $cc = new \stdClass();
        
        $cc->table = $table;
        $cc->namespace = $namespace;
        $cc->class = $tConfig['class'];
        $cc->comment = $tConfig['comment'] ?? $tConfig['class'];
        $cc->extends = $tConfig['extends'] ?? '\\'.\AbraD450\MappedDatabase\Entity\Entity::class;
        $cc->properties = [];
        $cc->references = [];
        $cc->collections = [];
        $cc->methods = [];
        
        $propsConfig = isset($tConfig['properties']) ? $tConfig['properties'] : [];
        
        $struct = $db->getStructure();
        $columns = $struct->getColumns($table);

        // sort columns alphabetically - to make sure on every machine it has the same output
        usort($columns, fn($a, $b) => $a['name'] < $b['name'] ? -1 : ($a['name'] > $b['name'] ? 1 : 0));
        $generatedColumns = [];
        foreach($columns as $col) {
            $colName = $col['name'];
            $pConfig = isset($propsConfig[$colName]) ? $propsConfig[$colName] : [];

            // Skip this property
            if(!empty($pConfig['skip'])) {
                continue;
            }
            $generatedColumns[$colName] = true;

            $prop = new \stdClass();
            
            $prop->col = $col['name'];
            $prop->nullable = $col['nullable'];
            $prop->key = $col['primary'];
            
            $dataType = $pConfig['type'] ?? $adapter->convertType($col['nativetype'], $col['vendor']);

            // Event on class generation - listeners might add methods, comments, etc ...
            $this->onDataType($dataTypeEventData = new DataTypeEventData(
                $adapter->getDatabaseIdent(),
                $col['table'],
                $col['name'],
                $dataType,
                $col['vendor']
            ));

            $prop->type = $dataTypeEventData->dataType;
            $prop->name = $pConfig['name'] ?? $this->convertName($colName);
            
            $prop->access = $pConfig['access'] ?? 'rw';
            $prop->searchable = $pConfig['searchable'] ?? FALSE;
            $prop->format = $pConfig['format'] ?? NULL;
            
            $prop->comment = $pConfig['comment'] ?? NULL;
            
            $prop->nativeType = strtolower($col['nativetype']);
            $prop->json = $prop->nativeType === 'json' ? (
                    isset($pConfig['json']) && in_array($pConfig['json'], ['object','array']) ?
                        $pConfig['json'] :
                        'array'
                ) : null;
            
            $prop->virtual = false;

            $cc->properties[] = $prop;            
        }
        
        // VIRTUAL properties
        foreach($propsConfig as $propName => $propConfig) {
            // was it already generated by columns colection?
            if(isset($generatedColumns[$propName])) {
                continue;
            }
            
            if(!empty($propConfig['virtual'])) {
                $prop = new \stdClass();
                $prop->col = null;
                $prop->nullable = null;
                $prop->key = null;
                $prop->type = $propConfig['type'] ?? 'mixed';
                $prop->name = $propConfig['name'] ?? $propName;
                $prop->access = $propConfig['access'] ?? 'r';
                $prop->nativeType = null;
                $prop->format = null;
                $prop->comment = $propConfig['comment'] ?? null;
                $prop->searchable = false;
                $prop->virtual = true;
                $cc->properties[] = $prop;
            }
        }
                
        // REF - references
        $belongsTo = $struct->getBelongsToReference($table);
        // sort by keys
        ksort($belongsTo);
        foreach($belongsTo as $btColumn => $btTable) {
            
            $btConfig = isset($tConfig['references']) && isset($tConfig['references'][$btColumn]) ? $tConfig['references'][$btColumn] : [];
            
            $btTable = $this->convertReferencedTable($btTable);
            
            $btTableConfig = $this->findTableConfig($connectionName, $btTable);
            if(!isset($btTableConfig)) {
                // Referenced table is not configured, skip
                continue;
            }
            
            if(!isset($btTableConfig['namespace'])) {
                throw new Nette\InvalidArgumentException("Missing key 'namespace' for table '{$btTable}' definition.");
            }            
            if(!isset($btTableConfig['config']['class'])) {
                throw new Nette\InvalidArgumentException("Missing key 'class' for table '{$btTable}' definition.");
            }
            
            $refProp = new \stdClass();
            $refProp->col = $btColumn;
            $refProp->ref = $btTable;
            $refProp->type = '\\'.trim($btTableConfig['namespace'], '\\').'\\'.ltrim($btTableConfig['config']['class'],'\\');
            $refProp->name = $btConfig['name'] ?? $this->convertRefName($btColumn);
            $refProp->comment = $btConfig['comment'] ?? NULL;

            $cc->references[] = $refProp;
        }
        
        // RELATED - collections
        // must be defined manually (not all has-many references should be generated
        // $hasMany = $struct->getHasManyReference($table);
        if(isset($tConfig['collections'])) {
            foreach($tConfig['collections'] as $colTable => $colDef) {
                foreach($colDef as $colThrough => $colConfig) {
                    
                    if(!isset($colConfig['name'])) {
                        throw new Nette\InvalidArgumentException("Missing key 'name' for collection property '{$table} -> {$colTable}:{$colThrough}' definition.");
                    }
                    
                    $colTableConfig = $this->findTableConfig($connectionName, $colTable);
                    if(!isset($colTableConfig)) {
                        // Referenced table is not configured, skip
                        continue;
                    }
                    
                    if(!isset($colTableConfig['namespace'])) {
                        throw new Nette\InvalidArgumentException("Missing key 'namespace' for table '{$colTable}' definition.");
                    }            
                    if(!isset($colTableConfig['config']['class'])) {
                        throw new Nette\InvalidArgumentException("Missing key 'class' for table '{$colTable}' definition.");
                    }
                    
                    $colProp = new \stdClass();
                    $colProp->col = $colThrough;
                    $colProp->related = $colTable;
                    $colProp->type = '\\'.trim($colTableConfig['namespace'], '\\').'\\'.ltrim($colTableConfig['config']['class'], '\\');
                    $colProp->name = $colConfig['name'];
                    $colProp->comment = $colConfig['comment'] ?? NULL;
                    $colProp->condition = $colConfig['condition'] ?? NULL;

                    $cc->collections[] = $colProp;
                }
            }
        }
        return $cc;
    }

    protected function findTableConfig(string $connectionName, string $table): ?array
    {
        if(empty($this->connections[$connectionName])) {
            return null;
        }
        foreach($this->connections[$connectionName]->entities as $entitySet) {
            if(isset($entitySet->config[$table])) {
                return [
                    'namespace' => $entitySet->namespace,
                    'output' => $entitySet->output,
                    'config' => $entitySet->config[$table]
                ];
            }
        }
        return null;
    }
    

    
    
    protected function convertName(string $name): string
    {
        // upper case names like MODE_ID od CONSIGNEE_NAME will be lowercased and then processed to consigneeName
        if(preg_match('/^[A-Z0-9_\-]+$/', $name)) {
            $name = strtolower($name);
        }
        // some_name_from_db => someNameFromDb
        return lcfirst(str_replace('_', '', ucwords($name, '_')));
    }

    protected function convertRefName(string $name): string
    {
        $n = $this->convertName($name);
        if(str_ends_with(strtolower($n), 'id')) {
            return substr($n, 0, -2);
        }
        return $n.'Ref';
    }
    
    protected function convertReferencedTable(string $tableName): string
    {
        $tableNameParts = explode('.', $tableName);
        return end($tableNameParts);
    }


    
    
    protected function writeClassFile(\stdClass $cc, string $dir): string
    {
        if(!is_dir($dir)) {
            throw new Nette\DirectoryNotFoundException("Directory '{$dir}' not found!");
        }
        $dir = rtrim($dir, '/').'/';
        $file = $dir.$cc->class.'.php';
        
        $phpFile = new Nette\PhpGenerator\PhpFile();
        $namespace = $phpFile->addNamespace($cc->namespace);
        
        $reflection = new \ReflectionClass(Attributes\Table::class);
        $attrsNamespace = $reflection->getNamespaceName();        
        
        $namespace->addUse($attrsNamespace);
        
        $class = $namespace->addClass($cc->class);
        $class->addComment($cc->comment."\n");
        $class->setExtends($cc->extends);
        $class->addAttribute(Attributes\Table::class, ['name' => $cc->table]);
        
        // Class PROPERTIES
        foreach($cc->properties as $p) {
            $propAttrs = [
                'name' => $p->name,
                'col' => $p->col,
            ];

            if($p->key) {
                $propAttrs['key'] = TRUE;
            }
            if($p->nullable) {
                $propAttrs['nullable'] = TRUE;
            }
            if($p->searchable) {
                $propAttrs['searchable'] = TRUE;
            }
            if($p->format) {
                $propAttrs['format'] = $p->format;
            }
            if($p->nativeType) {
                $propAttrs['nativeType'] = $p->nativeType;
            }            
            if($p->virtual) {
                $propAttrs['virtual'] = $p->virtual;
            }
            
            if($p->type) {
                $propAttrs['type'] = $p->type;
            }
            if($p->access) {
                $propAttrs['access'] = $p->access;
            }
            if($p->comment) {
                $propAttrs['comment'] = $p->comment;
            }
            if($p->json) {
                $propAttrs['json'] = $p->json;
            }

            $class->addAttribute(Attributes\Property::class, $propAttrs);

            // add comment for autocompletion, access and type 
            $comment = '@property';
            $comment .= ($p->access === 'r' ? '-read' : ($p->access === 'w' ? '-write' : '')).' ';
            $comment .= $p->type.' ';
            $comment .= '$'.$p->name;
            if($p->comment !== NULL) {
                $comment .= ' '.$p->comment;
            }
            
            $class->addComment($comment);
        }
        
        $class->addComment(""); // Space between properties and references
        
        // Class REFERENCES
        foreach($cc->references as $r) {
            $propAttrs = [
                'name' => $r->name,
                'type' => $r->type,
                'ref' => $r->ref,
                'through' => $r->col
            ];
            
            $class->addAttribute(Attributes\Property::class, $propAttrs);
            
            // add comment for autocompletion, access and type 
            $comment = '@property-read ';
            $comment .= $r->type.' ';
            $comment .= '$'.$r->name;
            if($r->comment !== NULL) {
                $comment .= ' '.$r->comment;
            }
            
            $class->addComment($comment);
        }
        
        $class->addComment(""); // Space between references and collections
        
        // Class COLECTIONS
        foreach($cc->collections as $c) {
            $propAttrs = [
                'name' => $c->name,
                'type' => $c->type,
                'related' => $c->related,
                'through' => $c->col,
            ];
            if(isset($c->condition)) {
                $propAttrs['condition'] = $c->condition;
            }
            
            $class->addAttribute(Attributes\Property::class, $propAttrs);            
            
            // add comment for autocompletion, access and type 
            $comment = '@property-read ';
            $comment .= \AbraD450\MappedDatabase\Table\Selection::class.' '; // $c->type.'[] ';
            $comment .= '$'.$c->name;
            if($c->comment !== NULL) {
                $comment .= ' '.$c->comment;
            }
            
            $class->addComment($comment);            
        }
        
        // Event on class generation - listeners might add methods, comments, etc ...
        $this->onClass(new ClassEventData($class, $namespace, $phpFile, $cc));

        Nette\Utils\FileSystem::write($file, (string)$phpFile);
        
        return $file;
    }
    
}
