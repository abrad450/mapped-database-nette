<?php

namespace AbraD450\MappedDatabase\Generator;

use Nette;

use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;
use Nette\PhpGenerator\PhpFile;

/**
 * onClass Event Data
 * 
 * @property-read ClassType $class
 * @property-read PhpNamespace $namespace
 * @property-read PhpFile $phpFile
 * @property-read \stdClass $classContent
 */
class ClassEventData
{
    use Nette\SmartObject;
    
    public function __construct(
            private ClassType $class,
            private PhpNamespace $namespace,
            private PhpFile $phpFile,
            private \stdClass $classContent
        )
    {
    }
    
    public function getClass(): ClassType
    {
        return $this->class;
    }

    public function getNamespace(): PhpNamespace
    {
        return $this->namespace;
    }

    public function getPhpFile(): PhpFile
    {
        return $this->phpFile;
    }

    public function getClassContent(): \stdClass
    {
        return $this->classContent;
    }


}