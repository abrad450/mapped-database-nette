<?php

namespace AbraD450\MappedDatabase\Generator\Adapters;

interface GeneratorAdapter
{
    /**
     * Convert DB type from Nette Database Reflection to PHP type
     */
    public function convertType(string $nativeType, array $vendor): string;

    /**
     * Returns database ident (mysql, pgsql ...)
     */
    public function getDatabaseIdent(): string;
}