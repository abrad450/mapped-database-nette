<?php

namespace AbraD450\MappedDatabase\Generator\Adapters;

class FirebirdGeneratorAdapter extends GenericGeneratorAdapter
{

    protected static $typeMap = [
        'text' => 'string',             
        'short' => 'int',
        'long' => 'int',
        'quad' => 'int',
        'float' => 'float',
        'double' => 'float',
        'timestamp' => '\DateTime',
        'varying' => 'string',
        'blob' => 'string',
        'cstring' => 'string',
        'blob_id' => 'string',
        'date' => '\DateTime',
        'time' => '\DateTime',
        'int64' => 'int',
        'boolean' => 'bool'
    ];

    public function convertType(string $nativeType, array $vendor): string
    {
        $t = strtolower($nativeType ?? 'string');

        // NUMERIC / DECIMAL
        if($vendor['fieldsubtype'] ?? 0 !== 0 && $t === 'int64') {
            return 'float';
        }
        
        return self::$typeMap[$t] ?? $t;
    }
}