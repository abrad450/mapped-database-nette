<?php

namespace AbraD450\MappedDatabase\Table;

/**
 * ISelection - just to "GROUP" Selection and GRoupedSelection
 */
interface ISelection
{
    
    /**
     * Returns Entity class name for current table
     * 
     * @return string
     */    
    public function getEntityName(): string;
    
}
