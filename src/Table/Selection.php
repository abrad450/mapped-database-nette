<?php

namespace AbraD450\MappedDatabase\Table;

use AbraD450\MappedDatabase\EntityMap;

use Nette\Database\Explorer;
use Nette\Database\IConventions;

/**
 * Selection
 */
class Selection extends \Nette\Database\Table\Selection implements ISelection
{
    use SelectionTrait;
    
    protected EntityMap $entityMap;
        
    public function __construct(
            EntityMap $entityMap, 
            Explorer $explorer, 
            IConventions $conventions, 
            string $tableName, 
            \Nette\Caching\IStorage $cacheStorage = null,
            $wasAggregationCalled = FALSE)
    {
        parent::__construct($explorer, $conventions, $tableName, $cacheStorage);
        $this->wasAggregationCalled = $wasAggregationCalled;
        $this->entityMap = $entityMap;
    }
    

    
}
