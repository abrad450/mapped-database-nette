<?php

namespace AbraD450\MappedDatabase\Table;

use Nette;
use Nette\Database\Table\ActiveRow;
use AbraD450\MappedDatabase\MapException;

/**
 * Selection Trait
 */
trait SelectionTrait
{
    
    protected $wasAggregationCalled;
    
    protected $pureRequested;
    
    protected function createRow(array $row): ActiveRow
	{
        if($this->wasAggregationCalled || $this->pureRequested) {
            return parent::createRow($row);
        }
        if(!isset($this->entityMap[$this->name])) {
            throw new MapException("No 'table->entity' mapping for table '{$this->name}' found in configuration", MapException::MAPPING_NOT_FOUND);
        }
        if(!class_exists($this->entityMap[$this->name])) {
            throw new MapException('Class "'.$this->entityMap[$this->name].'" not found for table "'.$this->name.'".', MapException::ENTITY_CLASS_NOT_FOUND);
        }
        $class = $this->entityMap[$this->name];
        return new $class($row, $this);
	}
    
	protected function createGroupedSelectionInstance(string $table, string $column): \Nette\Database\Table\GroupedSelection
	{
		return new GroupedSelection($this->entityMap, $this->explorer, $this->conventions, $table, $column, $this, $this->cache ? $this->cache->getStorage() : null, $this->wasAggregationCalled);
	}
    
    public function getReferencedTable(ActiveRow $row, ?string $table, string $column = null): Nette\Database\Table\ActiveRow|false|null
    {
        return parent::getReferencedTable($row, $table, $column);
    }
    
    public function getReferencingTable(string $table, string $column = null, $active = null): ?\Nette\Database\Table\GroupedSelection
    {
        return parent::getReferencingTable($table, $column, $active);
    }
    
	public function createSelectionInstance(string $table = null): \Nette\Database\Table\Selection
	{
		return new Selection($this->entityMap, $this->explorer, $this->conventions, $table ?: $this->name, $this->cache ? $this->cache->getStorage() : null, $this->wasAggregationCalled);
	}
 
    /**
     * Returns Entity class name for current table
     * 
     * @return string
     */
    public function getEntityName(): string
    {
        $tableName = $this->getName();
        if(!isset($this->entityMap[$tableName])) {
            throw new MapException("No 'table->entity' mapping for table '{$tableName}' found in configuration", MapException::MAPPING_NOT_FOUND);
        }
        return $this->entityMap[$tableName];
    }

	/**
	 * Executes aggregation function.
	 * @param  string  $function  select call in "FUNCTION(column)" format
	 * @return mixed
	 */
	public function aggregation(string $function, ?string $groupFunction = null): mixed
	{
        $this->wasAggregationCalled = TRUE;
        $result = parent::aggregation($function, $groupFunction);
        $this->wasAggregationCalled = FALSE;
        return $result;
	}
    
    
	/**
	 * @inheritDoc
	 */
	public function fetchAssoc(string $path): array
	{
        $rows = array_map(fn($entity) => $entity->toArray(), $this->fetchAll());
		return Nette\Utils\Arrays::associate($rows, $path);
	}

    
    
    // -------------------------------------------------------------------------
    
    
    
	/**
	 * @inheritDoc
	 */
	public function pureFetchAssoc(string $path): array
	{
        $this->pureRequested = true;
        $rows = array_map(fn($entity) => $entity->toArray(), $this->fetchAll());
		$result = Nette\Utils\Arrays::associate($rows, $path);
        $this->pureRequested = false;
        return $result;
	}    
    
	/**
	 * Fetches all rows as associative array.
	 * @param  string|int  $key  column name used for an array key or null for numeric index
	 * @param  string|int  $value  column name used for an array value or null for the whole row
	 */
	public function pureFetchPairs($key = null, $value = null): array
	{
        $this->pureRequested = true;
		$pairs = Nette\Database\Helpers::toPairs($this->fetchAll(), $key, $value);
        $this->pureRequested = false;
        return $pairs;
	}

	/**
	 * Fetches all rows.
	 * @return ActiveRow[]
	 */
	public function pureFetchAll(): array
	{
        $this->pureRequested = true;
		$arr = iterator_to_array($this);
        $this->pureRequested = false;
        return $arr;
	}    
    
}
