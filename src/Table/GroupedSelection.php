<?php

namespace AbraD450\MappedDatabase\Table;

use Nette;
use Nette\Database\IConventions;
use Nette\Caching\IStorage;

use AbraD450\MappedDatabase\EntityMap;

/**
 * GroupedSelection
 */
class GroupedSelection extends Nette\Database\Table\GroupedSelection implements ISelection
{
    use SelectionTrait;
    
    protected EntityMap $entityMap;

	/**
	 * Creates filtered and grouped table representation.
	 */
    public function __construct(
            EntityMap $entityMap, 
            \AbraD450\MappedDatabase\Explorer $explorer, 
            IConventions $conventions, 
            string $tableName, 
            string $column, 
            $refTable, 
            IStorage $cacheStorage = null,
            $wasAggregationCalled = FALSE)
	{
        if(!($refTable instanceof Selection || $refTable instanceof GroupedSelection)) {
            throw new Nette\InvalidArgumentException('Argument $refTable must be either '.Selection::class.' or '.GroupedSelection::class);
        }

        parent::__construct($explorer, $conventions, $tableName, $column, $refTable, $cacheStorage);
        $this->entityMap = $entityMap;
        $this->wasAggregationCalled = $wasAggregationCalled;
	}
    
    
}