<?php

/**
 * MIT License
 * For full license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AbraD450\MappedDatabase\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

use AbraD450\MappedDatabase\Generator\EntityGenerator;

#[AsCommand(name: 'generate')]
class Generate extends Command
{
    
    protected static $defaultName = 'generate';
    
    private EntityGenerator $generator;

    public function __construct(EntityGenerator $generator, string $name = null)
    {
        parent::__construct($name);
        $this->generator = $generator;
    }
    
    /**
     * {@inheritDoc}
     *
     * @return void
     */
    protected function configure(): void
    {
        $this->setDescription('Generate entities')
            ->setHelp(
                <<<EOT
The <info>generate</info> command generates entities from configuration
                    
<info>mapped generate</info>
EOT
            );
    }

    /**
     * Rollback the migration.
     *
     * @param \Symfony\Component\Console\Input\InputInterface $input Input
     * @param \Symfony\Component\Console\Output\OutputInterface $output Output
     * @return int integer 0 on success, or an error code.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Generating entities:');
        $generated = $this->generator->generate();
        foreach($generated as $file) {
            $output->writeln("<info>{$file}</info>");
        }
        return 0;
    }
} 