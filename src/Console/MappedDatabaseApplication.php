<?php

namespace AbraD450\MappedDatabase\Console;

use AbraD450\MappedDatabase\Console\Command\Generate;
use AbraD450\MappedDatabase\Generator\EntityGenerator;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Phinx console application.
 *
 * @author Rob Morgan <robbym@gmail.com>
 */
class MappedDatabaseApplication extends Application
{
    /**
     * Initialize the Phinx console application.
     */
    public function __construct(EntityGenerator $generator)
    {
        parent::__construct('<info>Mapped Database</info>');

        $this->addCommands([
            new Generate($generator),
        ]);
    }

}
